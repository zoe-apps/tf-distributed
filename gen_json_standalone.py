# Copyright (c) 2016, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import sys
import os

APP_NAME = 'tf-standalone'
ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'core_limit': {
        'value': 4,
        'description': 'Core limit'
    },
    'memory_limit': {
        'value': 4 * (1024**3),
        'description': 'Memory limit (bytes)'
    }
}

REGISTRY = os.getenv("DOCKER_REGISTRY", default="docker-engine:5000")
REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

CUSTOM_IMAGE = REGISTRY + '/' + REPOSITORY + '/tensorflow:' + VERSION


def custom_tensorflow_service(memory_limit, core_limit):
    """
    :rtype: dict
    """
    service = {
        'name': "tf",
        'image': CUSTOM_IMAGE,
        'monitor': True,
        'resources': {
            "memory": {
                "min": memory_limit,
                "max": memory_limit
            },
            "cores": {
                "min": core_limit,
                "max": core_limit
            }
        },
        'ports': [],
        'environment': [],
        'volumes': [],
        'command': None,
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0,
        'replicas': 1
    }

    return service



if __name__ == '__main__':
    app = {
        'name': APP_NAME,
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 512,
        'services': [
            custom_tensorflow_service(options["memory_limit"]["value"], options["core_limit"]["value"])
        ]
    }

    json.dump(app, open("custom_tensorflow.json", "w"), sort_keys=True, indent=4)

    print("ZApp written")

